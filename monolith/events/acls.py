import requests
from django.http import JsonResponse
from .keys import OPEN_WEATHER_API_KEY
import json


def get_photo(city, state):
    # Create the URL for the request with the city and state
    url = "https://api.pexels.com/v1/search"
    # Create a dictionary for the headers to use in the request
    headers = {
        "Authorization": "XHF5dUjeVh2fOp3lU8n1YfVIjpQxpwmz1t41Q0jEmZtBlKsk9NH3MHP0"
    }
    params = {
        "per_page": 1,
        "query": f"{city} {state}",
    }
    response = requests.get(url, params=params, headers=headers)
    # Make the request
    clue = json.loads(response.content)
    # Parse the JSON response
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    try:
        return {
            "photo_url": clue["photos"][0]["src"]["original"],
        }
    except (KeyError, IndexError):
        return {"photo_url": None}

    # return JsonResponse({"picture_url": clue["photos"]["url"]})


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state

    # loc_url = "http://api.openweathermap.org/geo/1.0/direct?"
    # loc_params = {
    #     "q": f"{city}, {state}, US"
    #     "appid": OPEN_WEATHER_API_KEY,
    # }

    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response

    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
    pass
