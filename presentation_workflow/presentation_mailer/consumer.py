import json
import pika
import time
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
from django.core.mail import send_mail

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()

        channel.queue_declare(queue="presentation_approvals")
        channel.queue_declare(queue="presentation_rejections")

        def process_approval(ch, method, properties, body):
            message = json.loads(body)
            presenter_email = message["presenter_email"]
            presenter_name = message["presenter_name"]
            presentation_title = message["title"]

            subject = "Your presentation has been accepted"
            body = f"{presenter_name}, we're happy to tell you that your presentation {presentation_title} has been accepted"
            from_email = "admin@conference.go"

            send_mail(
                subject=subject,
                message=body,
                from_email=from_email,
                recipient_list=[presenter_email],
            )

            print(f"Email sent to {presenter_email}")

        def process_rejection(ch, method, properties, body):
            message = json.loads(body)
            presenter_email = message["presenter_email"]
            presenter_name = message["presenter_name"]
            presentation_title = message["title"]

            subject = "Your presentation has been rejected"
            body = f"{presenter_name}, we're sorry to tell you that your presentation {presentation_title} has been denied"
            from_email = "admin@conference.go"

            send_mail(
                subject=subject,
                message=body,
                from_email=from_email,
                recipient_list=[presenter_email],
            )

            print(f"Email sent to {presenter_email}")

        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
